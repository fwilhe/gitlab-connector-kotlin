package com.gitlab.fwilhe.gitlabconnectorkotlin

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class NamespacesKtTest {
    @Rule @JvmField var wireMockRule = WireMockRule()

    @Test fun getNamespacesReturnsTheCorrectArray() {
        // given
        val expectedNamespaces = listOf(Namespace(22, "sputnik", "sputnik", "user", "sputnik"))

        WireMock.stubFor(
                WireMock.get(WireMock.urlEqualTo("/api/v4/namespaces"))
                        .willReturn(WireMock.aResponse().withBody("""[{"id":22,"name":"sputnik","path":"sputnik","kind":"user","full_path":"sputnik"}]"""))
        )

        // when
        val namespaces = getNamespaces("http://localhost:8080")

        // then
        assertEquals(namespaces, expectedNamespaces)
        WireMock.verify(WireMock.getRequestedFor(WireMock.urlEqualTo("/api/v4/namespaces")))
    }
}