package com.gitlab.fwilhe.gitlabconnectorkotlin

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test


class VersionKtTest {

    @Rule @JvmField var wireMockRule = WireMockRule()

    @Test fun getVersionReturnsTheCorrectObject() {
        // given
        val expectedVersion = Version("1.0.0", "abcabc")

        stubFor(
                get(urlEqualTo("/api/v4/version"))
                        .willReturn(aResponse().withBody("""{"version":"1.0.0","revision":"abcabc"}"""))
        )

        // when
        val version = getVersion("http://localhost:8080")

        // then
        assertEquals(version, expectedVersion)
        verify(getRequestedFor(urlEqualTo("/api/v4/version")))
    }
}
