package com.gitlab.fwilhe.gitlabconnectorkotlin

import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.httpPost

data class CiLint(val status: String, val errors: List<String>)

fun postCiLint(domainName: String, ciFileContent: String): CiLint {
    data class RequestBody(val content: String)

    val (request, response, result) = "$domainName/api/v4/ci/lint"
            .httpPost()
            .body(mapper.writeValueAsBytes(RequestBody(ciFileContent)))
            .header(Pair("PRIVATE-TOKEN", Configuration.privateToken()), Pair("Content-Type", "application/json"))
            .responseString()
    return mapper.readValue<CiLint>(result.get())
}
