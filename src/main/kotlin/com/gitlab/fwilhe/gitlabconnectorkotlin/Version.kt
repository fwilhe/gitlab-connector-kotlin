package com.gitlab.fwilhe.gitlabconnectorkotlin

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.httpGet

data class Version(val version: String, val revision: String)

val mapper = jacksonObjectMapper()

fun getVersion(domainName: String): Version {
    val (request, response, result) = "$domainName/api/v4/version"
            .httpGet().header(Pair("PRIVATE-TOKEN", Configuration.privateToken())).responseString()
    return mapper.readValue<Version>(result.get())
}
