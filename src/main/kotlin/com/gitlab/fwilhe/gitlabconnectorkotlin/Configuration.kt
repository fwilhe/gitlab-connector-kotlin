package com.gitlab.fwilhe.gitlabconnectorkotlin

import com.typesafe.config.ConfigException
import com.typesafe.config.ConfigFactory
import java.io.File

object Configuration {
    var privateTokenCache: String = ""

    fun privateToken(): String {
        if (privateTokenCache.isEmpty()) {
            privateTokenCache = try {
                ConfigFactory.parseFile(File("config.properties")).getString("private-gitlab-token")
            } catch (e: ConfigException) {
                "no-token"
            }
        }

        return privateTokenCache
    }
}
