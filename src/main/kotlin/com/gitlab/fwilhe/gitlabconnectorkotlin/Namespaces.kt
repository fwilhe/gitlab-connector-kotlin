package com.gitlab.fwilhe.gitlabconnectorkotlin

import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.httpGet

data class Namespace(val id: Long, val name: String, val path: String, val kind: String,
                     val full_path: String)

fun getNamespaces(domainName: String): List<Namespace> {
    val (request, response, result) = "$domainName/api/v4/namespaces"
            .httpGet().header(Pair("PRIVATE-TOKEN", Configuration.privateToken())).responseString()
    return mapper.readValue<List<Namespace>>(result.get())
}
